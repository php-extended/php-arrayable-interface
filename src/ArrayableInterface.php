<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-arrayable-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Arrayable;

/**
 * ArrayableInterface interface file.
 * 
 * This represents a trait to objects to be able to be transformed to an array.
 * This is the first step to serialize objects into any output whichever the
 * format or encoding.
 * 
 * @author Anastaszor
 */
interface ArrayableInterface
{
	
	/**
	 * Returns a recursive array of primitives that represents the values of
	 * this object and the objects it relates to.
	 * 
	 * Null values should not be returned into this array hierarchy. Children
	 * may be recursive lists of primitives, or recursive maps of primitives,
	 * but integer and string keys should not be merged to facilitate the
	 * serialization between array-lists and array-objects.
	 * 
	 * Empty arrays should not be returned into this hierarchy, however, if
	 * this object has nothing to serialize, a root empty array at the top of
	 * the hierarchy is allowed to be the return of this method.
	 * 
	 * @return array<string, boolean|integer|float|string|array<integer, boolean|integer|float|string>|array<string, boolean|integer|float|string>>
	 */
	public function toArray() : array;
	
}
